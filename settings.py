class const(object):
    __slots__ = ()
    ATTACK = -1
    DEFENCE = 1

    ATTACK_COLOUR = 'r'
    DEFENCE_COLOUR = 'g'

    IN = 'IN'
    OUT = 'OUT'
    UNDEC = 'UNDEC'
    NULL = ''

    allLabels = [IN,OUT,UNDEC]

    IN_COLOUR = 'g'
    OUT_COLOUR = 'r'
    UNDEC_COLOUR = 'y'

    LABEL_FONT_SIZE = 16
    NODE_SIZE = 600


const = const()
