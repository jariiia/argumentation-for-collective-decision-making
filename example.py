from collectiveFunction import *
from generator import *

if __name__ == "__main__":

    G = nx.DiGraph()
    G.add_nodes_from(range(1,5),label=const.UNDEC)

    # Attacking edges
    G.add_edges_from([(1,4),(2,4)],color=const.ATTACK_COLOUR,label=const.ATTACK)

    # Defending edges
    G.add_edges_from([(1,2),(3,4)],color=const.DEFENCE_COLOUR,label=const.DEFENCE)

    G.nodes[1]['label'] = const.IN
    G.nodes[2]['label'] = const.IN
    G.nodes[3]['label'] = const.OUT
    G.nodes[4]['label'] = const.OUT

    # define target argument

    target = 4

    # define labellings

    L1 = {1:const.IN, 2:const.IN, 3: const.IN, 4:const.OUT}
    L2 = {1:const.OUT, 2:const.OUT, 3: const.IN, 4:const.IN}
    L3 = {1:const.IN, 2:const.OUT, 3: const.IN, 4:const.IN}

    # labelling profile

    profile = [L1,L2,L3]

    # drawProfile(G,profile,target,['L1','L2','L3'])

    # Compute collective labelling and collective decision over the target

    collective_labelling = compute_collective_labelling(G, profile, target, CF)
    decision = compute_collective_decision(collective_labelling, target)

    # Draw individual and collective labellings

    profile.append(collective_labelling)
    all_titles = ['L1', 'L2', 'L3', 'Collective decision']
    draw_profile(G, profile, target, all_titles)
