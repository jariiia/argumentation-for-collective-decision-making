import datetime as dt
from collectiveFunction import *
from generator import *
import matplotlib.pyplot as plt
import numpy as np

def generateAndSolve(problems,arguments,numberOfProfiles,connectivity):
    initNodeLabel = const.UNDEC
    nodeLabels = [const.IN,const.OUT,const.UNDEC]
    edgeLabels = [const.ATTACK,const.DEFENCE]
    solvedProblems = 0
    solvingTimes =[]
    while solvedProblems < problems:
        DAG = generate_random_dag(arguments, connectivity, initNodeLabel, edgeLabels)
        if DAG:
            profile = generate_profile(DAG, numberOfProfiles, nodeLabels)
            targetIndex = DAG.number_of_nodes() - 1
            begin = dt.datetime.now()
            collectiveLabelling = compute_collective_labelling(DAG, profile, targetIndex, CF)
            end = dt.datetime.now()
            solvingTime = (end - begin).total_seconds()
            solvedProblems += 1
            solvingTimes.append(solvingTime)
    return solvingTimes

if __name__ == "__main__":

    numberOfProblems = 2
    arguments = [100,200,300,400,500]

    # Effect of connectivity on solving time

    numberOfParticipants = 1000
    connectivity = [0.25,0.5,0.75]
    fig, ax = plt.subplots()
    print 'Computing sensitivity to density of target-oriented discussion framework...'
    for c in connectivity:
        print 'Density parameter: ',c
        times = []
        errors = []
        for i in arguments:
            solvingTimes = generateAndSolve(numberOfProblems,i,numberOfParticipants,c)
            times.append(np.mean(solvingTimes))
            errors.append(2.0*np.std(solvingTimes))
            print i, 'arguments:', np.mean(np.mean(solvingTimes)), '+-', 2.0*np.std(solvingTimes)
        if c == 0.25:
            ax.plot(arguments, times, 'r-', label='Low density')
            ax.errorbar(arguments, times, yerr=errors,fmt='ro')
        if c == 0.5:
            ax.plot(arguments, times, 'bs--', label='Medium density')
            ax.errorbar(arguments, times, yerr=errors,fmt='bo')
        if c == 0.75:
            ax.plot(arguments, times, 'g^:', label='High density')
            ax.errorbar(arguments, times, yerr=errors,fmt='go')
    plt.xlabel('Number of arguments')
    plt.ylabel('Time (s)')
    plt.title('Sensititivy to density')

    legend = ax.legend(loc='upper left', shadow=True)

    frame = legend.get_frame()
    frame.set_facecolor('0.90')

    for label in legend.get_texts():
        label.set_fontsize('large')

    for label in legend.get_lines():
        label.set_linewidth(1.5)  # the legend line width

    plt.savefig('DensitySensitive.png',bbox_inches='tight')
    plt.clf()

    # Effect of number of participants

    connectivity = 0.5
    numberOfParticipants = [1000,10000]
    fig, ax = plt.subplots()
    print 'Computing sensitivity to number of participants...'
    for participants in numberOfParticipants:
        print 'Number of participants: ',participants
        times = []
        errors = []
        for i in arguments:
            solvingTimes = generateAndSolve(numberOfProblems,i,participants,connectivity)
            times.append(np.mean(solvingTimes))
            errors.append(2.0*np.std(solvingTimes))
            print i, ' arguments:', np.mean(np.mean(solvingTimes)), '+-', 2.0*np.std(solvingTimes)
        if participants == 1000:
            ax.plot(arguments, times, 'r-', label='$10^3$ users')
            ax.errorbar(arguments, times, yerr=errors,fmt='ro')
        if participants == 10000:
            ax.plot(arguments, times, 'bs--', label='$10^4$ users')
            ax.errorbar(arguments, times, yerr=errors,fmt='bo')
        if participants == 100000:
            ax.plot(arguments, times, 'g^:', label='$10^5$ users')
            ax.errorbar(arguments, times, yerr=errors,fmt='go')
    plt.xlabel('Number of arguments')
    plt.ylabel('Time (s)')
    plt.title('Sensititivy to number of participants')

    legend = ax.legend(loc='upper left', shadow=True)

    frame = legend.get_frame()
    frame.set_facecolor('0.90')

    for label in legend.get_texts():
        label.set_fontsize('large')

    for label in legend.get_lines():
        label.set_linewidth(1.5)  # the legend line width

    plt.savefig('ParticipantSensitive.png',bbox_inches='tight')

