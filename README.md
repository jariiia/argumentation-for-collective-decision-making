# README #


### INTRODUCTION ###

This repository provides an implementation of a family of aggregation functions that allow to make collective decisions in argumentation-based debates. 
The repo implements the aggregation functions defined in the following paper:


[1] Ganzer-Ripoll, J., Criado, N., Lopez-Sanchez, M. et al. Combining Social Choice Theory and Argumentation: Enabling Collective Decision Making. Group Decis Negot 28, 127–173 (2019). https://doi.org/10.1007/s10726-018-9594-6

The implemenation allows to run the example employed throughout the paper and to reproduce the experiments in the paper.

### VERSION ###

1.1

### REQUIREMENTS ###

To run this repo, you will need:

* *Python 3.9*
* *Matplotlib 3.5.1* 
* *Networkx 2.6.2*
* *PyInterval*

### HOW TO RUN AN EXAMPLE AND REPRODUCE THE EXPERIMENTS IN [1]  ###

In order to run the example in [1], run:

> *python example.py*

In order to reproduce the experiments in [1], simply run:

> *python experiment.py*

with the default settings.


### REPO ADMIN ###

jar@iiia.csic.es