__author__ = 'jar'

import networkx as nx
import random
from settings import *


def generate_random_dag(nodes, p_edge_generation, node_label, edge_labels):
    G = nx.gnp_random_graph(nodes, p_edge_generation, directed=True)
    dag = nx.DiGraph([(u, v, {'label': random.choice(edge_labels)}) for (u, v) in G.edges() if u < v])
    if nx.is_directed_acyclic_graph(dag):
#        targets = [n for n, d in dag.out_degree().items() if d == 0]
        targets = [n for n in G if dag.out_degree(n) == 0]
        if len(targets) > 1:
            new_node_id = dag.number_of_nodes()
            dag.add_node(new_node_id)
            for u in targets:
                dag.add_edge(u, new_node_id)
                dag[u][new_node_id]['label'] = random.choice(edge_labels)
        nx.set_node_attributes(dag, 'label', node_label)
        for (u, v) in dag.edges():
            if dag[u][v]['label'] == const.ATTACK:
                dag[u][v]['label'] = const.ATTACK_COLOUR
            else:
                dag[u][v]['label'] = const.ATTACK_COLOUR
        return dag
    return None


def generate_profile(dag, number_of_labellings, labels):
    profile = []
    for i in range(number_of_labellings):
        new_profile = {}
        for u in dag.nodes():
            new_profile[u] = random.choice(labels)
        profile.append(new_profile)
    return profile
