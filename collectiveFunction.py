__author__ = 'jar'

import matplotlib.pyplot as plt
import networkx as nx


from settings import *


def attacking(G, argument):
    return [node for node in G if node in list(G.predecessors(argument)) and G[node][argument]['label'] == const.ATTACK]


def defenders(G, argument):
    return [node for node in G if node in list(G.predecessors(argument)) and G[node][argument]['label'] == const.DEFENCE]


def count_labels(G, labelling, arguments, label):
    counter = 0
    for a in arguments:
        if labelling[a] == label:
            counter = counter + 1
    return counter


def Pro(G,labelling,argument):
    d = defenders(G,argument)
    a = attacking(G,argument)
    return count_labels(G, labelling, d, const.IN) + count_labels(G, labelling, a, const.OUT)


def Con(G,labelling,argument):
    d = defenders(G,argument)
    a = attacking(G,argument)
    return count_labels(G, labelling, a, const.IN) + count_labels(G, labelling, d, const.OUT)


def direct_support(profile, argument, label):
    support = 0
    for labeling in profile:
        if labeling[argument] == label:
            support = support + 1
    return support


def coherent(G,labelling):
    is_coherent = True
    for argument in G:
        if labelling[argument] == const.IN:
            is_coherent = is_coherent and (Pro(G,labelling,argument) >= Con(G,labelling,argument))
        if labelling[argument] == const.OUT:
            is_coherent = is_coherent and (Pro(G,labelling,argument) <= Con(G,labelling,argument))
    return is_coherent

def cCoherent(G,labelling,c):
    is_coherent = True
    for argument in G:
        if labelling[argument] == const.IN:
            is_coherent = is_coherent and (Pro(G,labelling,argument) > Con(G,labelling,argument) + c)
        if labelling[argument] == const.OUT:
            is_coherent = is_coherent and (Pro(G,labelling,argument) < Con(G,labelling,argument) + c)
        if labelling[argument] == const.UNDEC:
            is_coherent = abs(Pro(G,labelling,argument) - Con(G,labelling,argument)) <= c
    return is_coherent

def getIndirectOpinion(G,labelling,argument):
    pros = Pro(G,labelling,argument)
    cons = Con(G,labelling,argument)
    if pros > cons:
        return 1
    elif pros < cons:
        return -1
    else:
        return 0

def get_direct_opinion(G, profile, argument):
    direct_positive_support = direct_support(profile, argument, const.IN)
    direct_negative_support = direct_support(profile, argument, const.OUT)
    if direct_positive_support > direct_negative_support:
        return 1
    elif direct_positive_support < direct_negative_support:
        return -1
    else:
        return 0

def Majority(G, profile, collective_labelling, argument):
    direct_positive_support = direct_support(profile, argument, const.IN)
    direct_negative_support = direct_support(profile, argument, const.OUT)
    if direct_positive_support > direct_negative_support:
        collective_labelling[argument] = const.IN
    elif direct_positive_support < direct_negative_support:
        collective_labelling[argument] = const.OUT
    else:
        collective_labelling[argument] = const.UNDEC

def OF(G, profile, collective_labelling, argument):
    pros = Pro(G, collective_labelling, argument)
    cons = Con(G, collective_labelling, argument)
    direct_positive_support = direct_support(profile, argument, const.IN)
    direct_negative_support = direct_support(profile, argument, const.OUT)
    if (direct_positive_support > direct_negative_support) or ((pros > cons) and (direct_positive_support == direct_negative_support)):
        collective_labelling[argument] = const.IN
    elif (direct_positive_support < direct_negative_support) or ((pros < cons) and (direct_positive_support == direct_negative_support)):
        collective_labelling[argument] = const.OUT
    else:
        collective_labelling[argument] = const.UNDEC

def SF(G, profile, collective_labelling, argument):
    pros = Pro(G, collective_labelling, argument)
    cons = Con(G, collective_labelling, argument)
    direct_positive_support = direct_support(profile, argument, const.IN)
    direct_negative_support = direct_support(profile, argument, const.OUT)
    if (pros > cons) or ((pros == cons) and (direct_positive_support > direct_negative_support)):
        collective_labelling[argument] = const.IN
    elif (pros < cons) or ((pros == cons) and (direct_positive_support < direct_negative_support)):
        collective_labelling[argument] = const.OUT
    else:
        collective_labelling[argument] = const.UNDEC

def CF(G, profile, collective_labelling, argument):
    indirect_opinion = getIndirectOpinion(G, collective_labelling, argument)
    direct_opinion = get_direct_opinion(G, profile, argument)
    aggregated_opinion = direct_opinion + indirect_opinion
    if aggregated_opinion > 0:
        collective_labelling[argument] = const.IN
    elif aggregated_opinion < 0:
        collective_labelling[argument] = const.OUT
    else:
        collective_labelling[argument] = const.UNDEC

def compute_collective_labelling(G, profile, target, AF):
    H = G.copy()
    collective_labelling ={}
    toVisit = [n for n in H if not(list(H.predecessors(n)))]
    while toVisit:
        argument = toVisit.pop(0)
        AF(G,profile,collective_labelling,argument)
        successors = list(H.successors(argument))
        for successor in successors:
            H.remove_edge(argument,successor)
            if not(list(H.predecessors(successor))):
                toVisit.append(successor)
    return collective_labelling

def compute_collective_decision(collective_labelling, argument):
    return collective_labelling[argument]

def draw_labelling(G, labelling, target, title):

    pos = nx.spring_layout(G)

    in_nodes = [node for node in G if labelling[node] == const.IN]
    out_nodes = [node for node in G if labelling[node] == const.OUT]
    undec_nodes = [node for node in G if labelling[node] == const.UNDEC]

    nx.draw_networkx_nodes(G,pos,
                        in_nodes,
                        node_size = const.NODE_SIZE,
                        node_color= const.IN_COLOUR)

    nx.draw_networkx_nodes(G,pos,
                       out_nodes,
                       node_size = const.NODE_SIZE,
                       node_color= const.OUT_COLOUR)

    nx.draw_networkx_nodes(G,pos,
                       undec_nodes,
                       node_size = const.NODE_SIZE,
                       node_color= const.UNDEC_COLOUR)

    # edges
    nx.draw_networkx_edges(G,pos)

    attack_edges = [(u,v) for (u,v) in G.edges() if G[u][v]['color'] == const.ATTACK_COLOUR]
    nx.draw_networkx_edges(G,pos,
                      attack_edges,
                      width=4,alpha=0.25,edge_color=const.ATTACK_COLOUR)

    defence_edges = [(u,v) for (u,v) in G.edges() if G[u][v]['color'] == const.DEFENCE_COLOUR]
    nx.draw_networkx_edges(G,pos,
                      defence_edges,
                      width=4,alpha=0.25,edge_color=const.DEFENCE_COLOUR)


    labels={}
    no_targets = [node for node in G if not node == target]
    for node in no_targets:
        labels[node] = r'$a' + str(node) + '$'
    labels[target]=r'$\tau$'

    nx.draw_networkx_labels(G,pos,labels,font_size=const.LABEL_FONT_SIZE)

    plt.title('Labeling : ' + title)
    plt.axis('off')
    plt.show()

def draw_profile(G, profile, target, titles):

    iter_titles = iter(titles)

    for labelling in profile:

        in_nodes = [node for node in G if labelling[node] == const.IN]
        out_nodes = [node for node in G if labelling[node] == const.OUT]
        undec_nodes = [node for node in G if labelling[node] == const.UNDEC]

        plt.figure()

        pos = nx.spring_layout(G)

        nx.draw_networkx_nodes(G,pos,
                            in_nodes,
                            node_size = const.NODE_SIZE,
                            node_color= const.IN_COLOUR)

        nx.draw_networkx_nodes(G,pos,
                           out_nodes,
                           node_size = const.NODE_SIZE,
                           node_color= const.OUT_COLOUR)

        nx.draw_networkx_nodes(G,pos,
                           undec_nodes,
                           node_size = const.NODE_SIZE,
                           node_color= const.UNDEC_COLOUR)

        # edges
        nx.draw_networkx_edges(G,pos)

        attack_edges = [(u,v) for (u,v) in G.edges() if G[u][v]['color'] == const.ATTACK_COLOUR]
        nx.draw_networkx_edges(G,pos,
                          attack_edges,
                          width=4,alpha=0.25,edge_color=const.ATTACK_COLOUR)

        defence_edges = [(u,v) for (u,v) in G.edges() if G[u][v]['color'] == const.DEFENCE_COLOUR]
        nx.draw_networkx_edges(G,pos,
                          defence_edges,
                          width=4,alpha=0.25,edge_color=const.DEFENCE_COLOUR)


        labels={}
        noTargets = [node for node in G if not node == target]
        for node in noTargets:
            labels[node] = r'$a' + str(node) + '$'
        labels[target]=r'$\tau$'

        nx.draw_networkx_labels(G,pos,labels,font_size=const.LABEL_FONT_SIZE)

        plt.title('Labeling : ' + next(iter_titles))
        plt.axis('off')
    plt.show()